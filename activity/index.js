console.log('Hello World');

// Function that accepts first name, last name, and age as arguments and print those details in the console as a single string.
function userProfile(firstName, lastName, age){
  console.log(firstName + ' ' + lastName + ' is ' + age + ' years of age.');
}

userProfile('John', 'Smith', '30');

// Create a function that will return a value and store it in a variable
function returnFunction() {
	console.log('This was printed inside of the function');
	return true; //Boolean
	console.log('This was printed inside of the function');
}

let isMarried = returnFunction();
console.log("The value of isMarried is: " + isMarried);